
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var primeri = ["97a8f644-cb93-4909-b6dd-fb6e010fbe06","6d968268-515c-467c-8ff0-3583369a95d4","f49516e3-8e5d-4c27-877a-ac3e580486b8"];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var age;
//defualt
var sex = "male";
var height;
var weight;
var prvič = true;
var myChart1;
var myChart2;
var myChart3;
var hujsaj;
function generirajPodatke(stPacienta) {
  var sessionId = getSessionId();
    var ime;
    var priimek;
    var datumRojstva = "1999-04-05";
    var ehrId;
    var sex;
    switch(stPacienta){
    	case 1:{
    		ime = "Jason";
    		priimek = "Todd";
    		datumRojstva = "1983-03-20";
    		sex = "male";
    	}break;
    	case 2:{
    		ime = "Alfred";
    		priimek = "Pennyworth";
    		datumRojstva = "1943-04-11";
    		sex = "male";
    	}break;
    	case 3:{
    		ime = "Damian";
    		priimek = "Wayne";
    		datumRojstva = "2004-07-05";
    		sex = "male";
    	}
    }
    /*$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});*/
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    headers: {"Ehr-Session": sessionId},
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId},{key: "sex", value:sex}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            headers: {"Ehr-Session": sessionId},
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    //console.log(ehrId);
		                    primeri[stPacienta-1]=ehrId;
		                    //console.log(primeri);
		                    var op = document.getElementById("preberiObstojeciEHR");
							var opt1 = document.getElementById('preberiObstojeciEHR').options[1];
							var opt2 = document.getElementById('preberiObstojeciEHR').options[2];
							var opt3 = document.getElementById('preberiObstojeciEHR').options[3];
							opt1.value = primeri[0];
							opt2.value = primeri[1];
							opt3.value = primeri[2];
							$("#preberiSporocilo").html("");
							$("#preberiEHRid").val($(op).val());
							var snack = document.getElementById("snackbar");
							snack.className="show";
							setTimeout(function(){ snack.className = snack.className.replace("show", ""); }, 3000);
							dodajMeritve(stPacienta);
		                }
		            },
		            error: function(err) {
		                console.log("error "+err);
		            }
		        });
		    }
		});
		return ehrId;

}
function generacijaPrimerov(){
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
	//console.log(primeri);

	
}

function dodajMeritve(stPacienta){
    var ehrId=primeri[stPacienta-1];
    var telesnaTeza;
    var telesnaVisina;
    var sessionId = getSessionId();
    switch(stPacienta){
    	case 1:{
    		telesnaTeza = "103";
    		telesnaVisina = "187";
    	}break;
    	case 2:{
    		telesnaTeza = "83";
    		telesnaVisina = "172";
    	}break;
    	case 3:{
    		telesnaTeza = "38";
    		telesnaVisina = "140";
    	}
    }
    /*$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});*/
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    headers: {"Ehr-Session": sessionId},
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		       //console.log(res);
		    },
		    error: function(err) {
		    	console.log(JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
}
function preberiVse(){
	document.getElementById("b3").innerHTML="Počakajte";
	//preberiEHRodBolnika();
	preberiMeritve();
	document.getElementById("skrito").style.visibility="hidden";
}
function preberiEHRodBolnika() {
	var sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();
    $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				console.log(party);
			},
			error: function(err) {
			}
	});
	
}

function preberiMeritve(){
	var sessionId = getSessionId();
	var ehrId = $("#preberiEHRid").val();
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
	    headers: {"Ehr-Session": sessionId},
	    success: function(data){
	    	var party = data.party;
	    	var td = new Date();
	    	var bd = new Date(party.dateOfBirth);
	    	var razlika = td-bd;
	    	age = Math.round(razlika/31556952000);
	    	document.getElementById("ime").innerHTML=party.firstNames;
	    	document.getElementById("priimek").innerHTML=party.lastNames;
	    	document.getElementById("rojstvo").innerHTML=party.dateOfBirth+ " ["+age+"]";
	    	$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "weight",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
					success: function (res) {
					    //console.log(res[0].weight+ " kg");
					    document.getElementById("teza").innerHTML=res[0].weight+ " kg";
					    weight = res[0].weight;
					},
					error: function(err) {
					    console.log("error");
					}
			});
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "height",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
					success: function (res) {
					    //console.log(res[0].height+ " cm");	
					    document.getElementById("visina").innerHTML=res[0].height+" cm";
					    height = res[0].height;
					    document.getElementById("b3").style.visibility="visible";
					    document.getElementById("b3").innerHTML="Analiziraj";
					},
					error: function(err) {
					    console.log("error");
					}
			});
			
	    },
	    
		error: function(err){
			//console.log(JSON.parse(err.responseText).userMessage);
		}
	});
}
function vrniBMR(){
	var BMR;
	if(sex == "male"){
		BMR = 13.7516*weight+5.0033*height-6.7550*age + 66.4730;
	}else if(sex == "female"){
		BMR = 9.5634*weight+1.8496*height-4.6756*age+655.0955;
	}
	return BMR;
}
function vrniBMI(){
	var BMI;
	BMI = weight/(height/100*height/100);
	return BMI;
	
}
function idealWeight(){
	var IBW;
	if(sex == "male"){
		IBW = 48+2.7*(height/2.54-60)
	}else if(sex == "female"){
		IBW = 45.5+2.2*(height/2.54-60)
	}
	return IBW;
}
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
$(document).ready(function(){
    $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

	
})
function RendAll(){
	RendTeze();
	RendBMI();
	RendBMR();
	if(prvič)
		prvič=false;
	prognoza();
	priporocilo();
	odprt=-1;
	document.getElementById("skrito").style.visibility="visible";
	document.getElementById("alertBox").style.display="block";
}
function prognoza(){
	var b = Math.round(vrniBMI());
	var bm = document.getElementById("bmi")
	var alert = document.getElementById("pozorRdec");
	var opozorilo = "Vaša telesna teža je glede na vašo starost in višino prevelika.";
	var barva;
	var stringBmi = b+" kg/m2, kar je ";
	if(b<18.5){
		opozorilo = "Vaša telesna teža je glede na vašo starost in višino premajhna."
		stringBmi+= b-18.5 + "kg/m2 pod priporočeno vrednostjo. Ste prelahki.";
		barva = 1;
	}else if(b>=18.5 && b<24.5){
		stringBmi+="v mejah priporočene vrednosti";
		opozorilo = "Vaša telesna teža je glede na vašo starost in višino v mejah normale."
		barva = 0;
	}else if(b>=24.5 && b<29){
		stringBmi+= b-24.5+" kg/m2 nad priporočeno vrednostjo. Malo ste pretežki."
		barva = 2;
	}else{
		barva = 1;
		stringBmi+= b-24.5 +" kg/m2 nad priporočeno vrednostjo. Vaše stanje je že zdravju škodujoče."
	}
	bm.innerHTML=stringBmi;
	var iW = Math.round(idealWeight());
	var it = document.getElementById("idteza");
	it.innerHTML=iW;
	var tez = document.getElementById("tez");
	var stringTez = " kg, bi morali ";
	hujsaj;
	if(weight>iW){
		stringTez+="zgubiti "+(weight-iW)+" kg mase."
		hujsaj = true;
	}else if(iW>weight){
		stringTez+="pridobiti "+(iW-weight)+" kg mase."
		hujsaj = false;
	}else{
		stringTez+="vam ni potrebno storiti ničesar"
	}
	tez.innerHTML=stringTez;
	var ted = document.getElementById("tedni");
	var stringTedni;
	if(hujsaj){
		var tedniM = (weight-iW)/2;
		var tednim = (weight-iW);
		stringTedni = "<b>"+tedniM + "</b> do <b>"+tednim+ "</b> tednih.";
	}else{
		var tedniM = (iW-weight)/2;
		var tednim = (iW-weight);
		stringTedni ="<b>"+tedniM+ "</b> do <b>"+tednim+ "</b> tednih.";
	}
	ted.innerHTML=stringTedni;
	alert.innerHTML=opozorilo;
	var boxi = document.getElementById("alertBox");
	if(barva==0){
		boxi.style.backgroundColor="green";
	}else if(barva==2){
		boxi.style.backgroundColor="orange";
	}else if(barva==1){
		boxi.style.backgroundColor="red";
	}
}
function RendBMI(){
	var BMI = Math.round(vrniBMI());
	if(prvič){
		var ctx = document.getElementById("myChart2");
		myChart2 = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ["BMI"],
	        datasets: [{
	            label: 'kg/m2',
	            data: [BMI],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255,99,132,1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
		});
	}else{
		myChart2.data.datasets.forEach((dataset)=>{
			dataset.data.pop();
		});
	
    	myChart2.data.datasets.forEach((dataset) => {
        	dataset.data.push(BMI);
    	});
    	myChart2.update();
		
	}
}
function RendBMR(){
	var BMR = Math.round(vrniBMR());
	if(prvič){
		var ctx = document.getElementById("myChart3");
		myChart3 = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ["BMR"],
	        datasets: [{
	            label: 'J/(h*kg)',
	            data: [BMR],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255,99,132,1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
		});	
	}else{
		myChart3.data.datasets.forEach((dataset)=>{
			dataset.data.pop();
		});
	
    	myChart3.data.datasets.forEach((dataset) => {
        	dataset.data.push(BMR);
    	});
    	myChart3.update();
	}
}
function RendTeze(){
	var idealnaTeza = Math.round(idealWeight());
	if(prvič){
	var ctx = document.getElementById("myChart");
	myChart1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Teža", "Idealna teža"],
        datasets: [{
            label: 'kg',
            data: [weight,idealnaTeza],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
	});
	}else{
	myChart1.data.datasets.forEach((dataset)=>{
		dataset.data.pop();
	});
	myChart1.data.datasets.forEach((dataset)=>{
		dataset.data.pop();
	});
	
    myChart1.data.datasets.forEach((dataset) => {
        dataset.data.push(weight);
    });
    myChart1.data.datasets.forEach((dataset) => {
        dataset.data.push(idealnaTeza);
    });
	myChart1.update();
	}

}

var slave = [];
var odprt= -1;
function priporocilo(){
	var search1;
	var search2;
	if(hujsaj&&!(age<20)){
		search1 = "salad";
		search2 = "soup";
	}else{
		search1 = "steak";
		search2 = "fish";
	}
	var master = [];
	var response1;
	var response2;
	
	/*var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "https://www.themealdb.com/api/json/v1/1/search.php?s=steak", false);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(null);
    var response1 = JSON.parse(xhttp.responseText);
    //console.log(response1.meals[0]);
    var xhttp2 = new XMLHttpRequest();
    xhttp2.open("GET", "https://www.themealdb.com/api/json/v1/1/search.php?s=steak", false);
    //xhttp.setRequestHeader("Content-type", "application/json");
    xhttp2.send(null);
    var response2 = JSON.parse(xhttp2.responseText);
    console.log(response1);
    console.log(response2);*/
    
	$.ajax({
        type: "GET",
        url: 'https://www.themealdb.com/api/json/v1/1/search.php?s='+search1,
        async: false,
        success: function (res) {
        	response1 = res.meals;
        	$.ajax({
		        type: "GET",
		        url: 'https://www.themealdb.com/api/json/v1/1/search.php?s='+search2,
		        async: false,
		        success:function(res2){
		        	response2=res2.meals;
		        	var len = response1.length+response2.length;
				    for(var i = 0;i<response1.length;i++){
				    	master[i]= response1[i].strMeal;
				    	var ingredients = response1[i].strIngredient1+", "+response1[i].strIngredient2+", "+response1[i].strIngredient3+", "+response1[i].strIngredient4;
				    	slave[i] = [response1[i].strArea,ingredients,response1[i].strYoutube];
				    }
				    var j = response1.length;
				    for(var i = 0;i<response2.length;i++){
				    	master[j]= response2[i].strMeal;
				    	var ingredients = response2[i].strIngredient1+", "+response2[i].strIngredient2+", "+response2[i].strIngredient3+", "+response2[i].strIngredient4;
				    	slave[j] = [response2[i].strArea,ingredients,response2[i].strYoutube];
				    	j++;
				    }
				    var seznam = document.getElementById("seznam");
				    var dseznam ="<ul id =\"master\">";
					for(var i = 0;i<master.length;i++){
						dseznam+= "<li id=sez"+i+">"+"<a href=\"#\" onClick=\"dodaj("+i+")\">"+master[i]+"</a></li>";
					}
					dseznam+="</ul>";
					seznam.innerHTML=dseznam;
		        
		        }
		    });
        }, 
        error: function(err) {
        			//failsafe v primeru če bi api nehu delat, bo vsaj master-slave sistem še deloval
        			master = ["CE ME VIDIŠ, API NE DELUJE VEČ","CE ME VIDIŠ, API NE DELUJE VEČ","CE ME VIDIŠ, API NE DELUJE VEČ","CE ME VIDIŠ, API NE DELUJE VEČ","CE ME VIDIŠ, API NE DELUJE VEČ"]
					for(var i = 0;i<master.length;i++){
						slave[i]= ["Api nehal delovati ("+i+")","Tukaj so sestavine aha ("+i+")","www.genericLinkKerApiNeDela_neKliknt.com"];
					}
					var seznam = document.getElementById("seznam");
				    var dseznam ="<ul id =\"master\">";
					for(var i = 0;i<master.length;i++){
						dseznam+= "<li id=sez"+i+">"+"<a href=\"#\" onClick=\"dodaj("+i+")\">"+master[i]+"</a></li>";
					}
					dseznam+="</ul>";
					seznam.innerHTML=dseznam;
		},
    });
}
function dodaj(i){
	if(odprt>=0 || odprt==i){
		var parent = document.getElementById("sez"+odprt);
		var child = document.getElementById("s"+odprt);
		parent.removeChild(child);
	}
	if(odprt!=i){
		var id = "s"+i;
		var rezultat = "<ul id=\""+id+"\">";
		/*for(var j = 0;j<slave[i].length;j++){
			rezultat+="<li><b>" + slave[i][j] + "</b>"+"</li>"; 
		}*/
		rezultat+="<li> Poreklo: " + slave[i][0] +"</li>"; 
		rezultat+="<li> Nekaj sestavin potrebne za pripravo: " + slave[i][1] +"</li>"; 
		rezultat+="<li> Video priprave: <a href=\"" + slave[i][2] +"\"/a>"+slave[i][2]+"</a></li>"; 
		rezultat+="</ul>";
		var se=document.getElementById("sez"+i);
		se.innerHTML+=rezultat;
		odprt = i;
	}else{odprt=-1;}
}